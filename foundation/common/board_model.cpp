#include <list>
#include <cmath>
#include <functional>

#include "board_model.h"
#include "board_model_impl.inl"


namespace checkers
{

    /////////////////// class BoardModel ///////////////////////////
    BoardModel::BoardModel(unsigned rows, unsigned columns)
        : _impl(new BoardModel_Implementation(rows, columns))        
    {        
        if (rows != columns)
        {
            throw std::logic_error("Non square borders are not supported yet.");
        }
        reset();
    }

    BoardModel::~BoardModel()
    {
        delete _impl;
    }

    BoardModel::Move BoardModel::Move::construct_attack(const BoardPos& dest, const BoardPos& enemy) 
    {
        return Move(MoveType_Attack, dest, enemy);
    }

    BoardModel::Move BoardModel::Move::construct_move(const BoardPos& dest)
    {
        return Move(MoveType_Move, dest);
    }

    BoardModel::MoveType BoardModel::Move::type() const 
    { 
        return _type; 
    }

    const BoardPos& BoardModel::Move::position_to() const 
    { 
        return _destination; 
    }

    const BoardPos& BoardModel::Move::attacked_figure() const 
    { 
        return _attacked_figure; 
    }
    Cell& BoardModel::cell_at(const BoardPos& position)
    {
        return _impl->cell_at(position);
    }

    const Cell& BoardModel::cell_at(const BoardPos& position) const
    {
        return _impl->cell_at(position);
    }

    void BoardModel::reset()
    {        
        _impl->reset_board_layout();
        _impl->reset_figures();
    }

    unsigned BoardModel::white_figures_count() const
    {
        return _impl->_white_figures_count;
    }

    unsigned BoardModel::black_figures_count() const
    {
        return _impl->_black_figures_count;
    }
    
    void BoardModel::get_possible_moves(const BoardPos& from, MovesCollection& out_moves, bool attacks_only) const
    { 
        MovesCollection result_collection;
        int max_distance = 1; // holds allowed count of moves in one direction 
        // (1 - for ordinary figure and board size - 1 for queen)
        if (cell_at(from).is_queen())
        {
            max_distance = rows_count() - 1; // queen moves limited by board size
        }
        // find out vertical direction        
        BoardModel_Implementation::DirectionsList possible_directions = _impl->get_possible_directions(from);
        // try each direction 
        BoardModel_Implementation::DirectionsList::const_iterator dit = possible_directions.begin(), 
            directions_end = possible_directions.end();
        for (; dit != directions_end; ++dit)
        {
            const BoardModel_Implementation::Direction& direction = *dit;
            _impl->find_moves_for_direction(from, direction, max_distance, result_collection);
        }        
        _impl->filter_out_incompatible_moves(result_collection, attacks_only);        
        out_moves.swap(result_collection);
    }

    unsigned BoardModel::rows_count() const
    {
        return _impl->_rows;
    }

    unsigned BoardModel::columns_count() const
    {
        return _impl->_columns;
    }

    bool BoardModel::is_move_allowed(const BoardPos& from, const BoardPos& to, const MoveType& kind, bool attack_mode) const
    {
        MovesCollection moves_collection;        
        get_possible_moves(from, moves_collection, attack_mode);
        MovesCollection::const_iterator unused;
        return _impl->find_move_in_collection(moves_collection, from, to, kind, unused);
    }

    void BoardModel::make_move(const BoardPos& from, const Move& move, bool attack_mode)
    { 
        switch(move.type())
        {
        case MoveType_Move:
            {
                _impl->do_make_move(from, move, attack_mode);
                break;
            }
        case MoveType_Attack:
            {
                _impl->do_make_attack(from, move, attack_mode);
                break;
            }
        default:
            { // invalid/unknown move 
                assert("Invalid or unknown type specified" && false);
                break;
            }
        }        
    }

    void BoardModel::MovesCollection::add(const BoardModel::Move& move)
    {
        switch(move.type())
        {
        case MoveType_Attack:
            {
                ++_attacks;
            }
        case MoveType_Move:
            {
                _moves.push_back(move);
                break;
            }
        default:
            throw std::logic_error("Attempt to insert invalid move into collection");
        }
    }

    BoardModel::MovesCollection::const_iterator BoardModel::MovesCollection::begin() const 
    { 
        return _moves.begin(); 
    }

    BoardModel::MovesCollection::const_iterator BoardModel::MovesCollection::end() const 
    { 
        return _moves.end(); 
    }

    void BoardModel::MovesCollection::swap(BoardModel::MovesCollection& other)
    {
        _moves.swap(other._moves);
        std::swap(_attacks, other._attacks);
    }

    unsigned BoardModel::MovesCollection::count() const 
    {
        return static_cast<unsigned>(_moves.size());
    }

    unsigned BoardModel::MovesCollection::attacks_count() const
    {
        return _attacks;
    }

    unsigned BoardModel::MovesCollection::motions_count() const
    {
        return count() - attacks_count();
    }

    

} // namespace checkers


