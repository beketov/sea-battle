#ifndef CH_CANVAS_TWO_D_H
#define CH_CANVAS_TWO_D_H

namespace checkers 
{
    class Canvas2dIntf
    {
    public:
        virtual ~Canvas2dIntf() {}

        virtual void translate(int x, int y);
        virtual void draw_rect(int x, int y, int w, int h) = 0;
        virtual void draw_ellipse(int x, int y, int w, int h) = 0;
        virtual void set_brush_color(int r, int g, int b) = 0;
        
    protected:
        virtual void on_paint_begin() = 0;
        virtual void on_parint_end() = 0;
    };

} // namespace checkers 

#endif // CH_CANVAS_TWO_D_H
