#ifndef SB_CELL_H
#define SB_CELL_H

#include <cassert>

namespace seabattle
{
	enum class Cell
    {
        Empty = 0, 
		Life = 1, 
		Dead = 2, 
		Miss = 3
    };

	enum class Kind_of_ship
	{
		KS_FOUR_DECK = 0,
		KS_THREE_DECK_FIRST = 1,
		KS_THREE_DECK_SECOND = 2,
		KS_TWO_DECK_FIRST = 3,
		KS_TWO_DECK_SECOND = 4,
		KS_TWO_DECK_THIRD = 5,
		KS_ONE_DECK_FIRST = 6,
		KS_ONE_DECK_SECOND = 7,
		KS_ONE_DECK_THIRD = 8,
		KS_ONE_DECK_FOURTH = 9,
		KS_NONE = 10
	};

/*

	class Cell
	{
	public:
		
		Cell() : _flags(0), kind_of_ship(KS_NONE) { }
		
		bool is_empty() const { return (_flags & CellFlags_Empty)  != 0; }
		bool is_ship() const { return !is_empty(); }
		bool is_boner() const { return (_flags & CellFlags_Boner)  != 0; }
		bool is_ship_killed() const { return (_flags & CellFlags_Killed)  != 0; }

		void set_ship() { _flags = 0; (_flags |= CellFlags_Ship); (kind_of_ship |= KS_NONE); }
		void make_empty() { assert(is_ship()); _flags &= CellFlags_Empty; (_flags |= CellFlags_Empty); (kind_of_ship |= KS_NONE); }
		void set_boner() { _flags = 0;  _flags |= CellFlags_Boner; (kind_of_ship |= KS_NONE); }
		void set_killed_ship() { _flags = 0;  _flags |= CellFlags_Killed; (kind_of_ship |= KS_NONE); }

		bool isShipOfSuchKind( Kind_of_ship ship) const { return (kind_of_ship & ship)  != 0; }
		void setKindOfShip( Kind_of_ship new_kind_of_ship) { kind_of_ship = 0; kind_of_ship |= new_kind_of_ship; };

	private:

		int _flags;
		int kind_of_ship; 

	};
	*/
}
#endif