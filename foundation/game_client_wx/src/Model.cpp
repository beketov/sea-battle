#include "Model.h"

namespace seabattle
{
	Model::Model()
	{
		*my_field		= new Cell[10];
		*enemy_field	= new Cell[10];
		for(int i = 0; i<10; ++i)
		{
			my_field[i]		= new Cell[10];
			enemy_field[i]	= new Cell[10];
		}
		clearMyField();
		clearEnemyField();

		state = State::ST_PLACING_SHIPS;
		state_of_placing_ship = Kind_of_ship::KS_FOUR_DECK;
		current_number_of_decks = 0;
	}

	Model::~Model()
	{
		for(int i = 0;i<10; ++i)
		{
			delete my_field[i];
			delete enemy_field[i];
		}
		delete [] my_field;
		delete [] enemy_field;
	}

	void Model::resetCurrentNumberOfDecks()
	{
		current_number_of_decks = 0;
	}

	void Model::incrementCurrentNumberOfDecks()
	{
		++current_number_of_decks;
	}

	int Model::getCurrentNumberOfDecks() const
	{
		return current_number_of_decks;
	}

	State Model::getState() const
	{
		return state;
	}

	void Model::setState( State new_state)
	{
		state = new_state;
	}

	Kind_of_ship Model::getStateOfPlacingShip() const
	{
		return state_of_placing_ship;
	}

	void Model::setStateOfPlacingShip( Kind_of_ship new_state_of_placing_ship)
	{
		state_of_placing_ship = new_state_of_placing_ship;
	}

	bool Model::isCorrectCellCoordinates( int cell_x, int cell_y ) const
	{
		return ( cell_x >= 0 && cell_y >= 0 && cell_x < 10 && cell_y < 10 );
	}

	bool Model::isAnyShipAroundThisCell( int cell_x, int cell_y ) const
	{
		if(isCorrectCellCoordinates(cell_x-1,cell_y-1)&&(getMyCell(cell_x-1,cell_y-1)!=Cell::Empty))
			return true;
		if(isCorrectCellCoordinates(cell_x-1,cell_y)&&(getMyCell(cell_x-1,cell_y)!=Cell::Empty))
			return true;
		if(isCorrectCellCoordinates(cell_x-1,cell_y+1)&&(getMyCell(cell_x-1,cell_y+1)!=Cell::Empty))
			return true;
		if(isCorrectCellCoordinates(cell_x,cell_y-1)&&(getMyCell(cell_x,cell_y-1)!=Cell::Empty))
			return true;
		if(isCorrectCellCoordinates(cell_x,cell_y+1)&&(getMyCell(cell_x,cell_y+1)!=Cell::Empty))
			return true;
		if(isCorrectCellCoordinates(cell_x+1,cell_y-1)&&(getMyCell(cell_x+1,cell_y-1)!=Cell::Empty))
			return true;
		if(isCorrectCellCoordinates(cell_x+1,cell_y)&&(getMyCell(cell_x+1,cell_y-1)!=Cell::Empty))
			return true;
		if(isCorrectCellCoordinates(cell_x+1,cell_y+1)&&(getMyCell(cell_x+1,cell_y-1)!=Cell::Empty))
			return true;

		return false;
	}
	/*
	bool Model::isAnotherShipAroundThisCell( int cell_x, int cell_y, Kind_of_ship current_ship) const
	{
	if(isCorrectCellCoordinates(cell_x-1,cell_y-1)&&(!getMyCell(cell_x-1,cell_y-1).isShipOfSuchKind(current_ship)))
	return true;
	if(isCorrectCellCoordinates(cell_x-1,cell_y)&&(!getMyCell(cell_x-1,cell_y).isShipOfSuchKind(current_ship)))
	return true;
	if(isCorrectCellCoordinates(cell_x-1,cell_y+1)&&(!getMyCell(cell_x-1,cell_y+1).isShipOfSuchKind(current_ship)))
	return true;
	if(isCorrectCellCoordinates(cell_x,cell_y-1)&&(!getMyCell(cell_x,cell_y-1).isShipOfSuchKind(current_ship)))
	return true;
	if(isCorrectCellCoordinates(cell_x,cell_y+1)&&(!getMyCell(cell_x,cell_y+1).isShipOfSuchKind(current_ship)))
	return true;
	if(isCorrectCellCoordinates(cell_x+1,cell_y-1)&&(!getMyCell(cell_x+1,cell_y-1).isShipOfSuchKind(current_ship)))
	return true;
	if(isCorrectCellCoordinates(cell_x+1,cell_y)&&(!getMyCell(cell_x+1,cell_y-1).isShipOfSuchKind(current_ship)))
	return true;
	if(isCorrectCellCoordinates(cell_x+1,cell_y+1)&&(!getMyCell(cell_x+1,cell_y-1).isShipOfSuchKind(current_ship)))
	return true;

	return false;
	}
	*/
	Cell Model::getMyCell( int cell_x, int cell_y ) const
	{
		assert(Model::isCorrectCellCoordinates(cell_x, cell_y ));

		return my_field[cell_x][cell_y];
	}

	void Model::setMyCell( int cell_x, int cell_y, Cell new_cell )
	{
		assert(Model::isCorrectCellCoordinates(cell_x, cell_y ));

		my_field[cell_x][cell_y] = new_cell;
	}

	Cell** Model::getMyField() const
	{
		Cell* field_result[10];
		for(int i = 0;i<10; ++i)
			field_result[i] = new Cell[10];
		for(int i = 0; i<10; ++i)
			for(int j = 0; j<10; ++j)
				field_result[i][j] = my_field[i][j];
		return field_result;
	}

	void Model::clearMyField()
	{
		for(int i = 0; i<10; ++i)
			for(int j = 0; j<10; ++j)
				my_field[i][j] = Cell::Empty;
	}

	Cell Model::getEnemyCell( int cell_x, int cell_y ) const
	{
		assert(Model::isCorrectCellCoordinates(cell_x, cell_y ));

		return enemy_field[cell_x][cell_y];
	}

	void Model::setEnemyCell( int cell_x, int cell_y, Cell new_cell )
	{
		assert(Model::isCorrectCellCoordinates(cell_x, cell_y ));

		enemy_field[cell_x][cell_y] = new_cell;
	}

	Cell** Model::getEnemyField() const
	{
		Cell* field_result[10];
		for(int i = 0;i<10; ++i)
			field_result[i] = new Cell[10];

		for(int i = 0; i<10; ++i)
			for(int j = 0; j<10; ++j)
				field_result[i][j] = enemy_field[i][j];
		return field_result;
	}

	void Model::clearEnemyField()
	{
		for(int i = 0; i<10; ++i)
			for(int j = 0; j<10; ++j)
				enemy_field[i][j] = Cell::Empty;
	}

	bool Model::isMyCellEmpty( int cell_x, int cell_y ) const
	{
		return getMyCell(cell_x,cell_y) == Cell::Empty;
	}

	bool Model::isMyCellLife( int cell_x, int cell_y ) const
	{
		return getMyCell(cell_x,cell_y) == Cell::Life;
	}

	bool Model::isMyCellMiss( int cell_x, int cell_y ) const
	{
		return getMyCell(cell_x,cell_y) == Cell::Miss;
	}

	bool Model::isMyCellDead( int cell_x, int cell_y ) const
	{
		return getMyCell(cell_x,cell_y) == Cell::Dead;
	}

	bool Model::isAllMyShipsDead() const
	{
		for(int i = 0; i<10; ++i)
			for(int j = 0; j<10; ++j)
				if(my_field[i][j] != Cell::Dead)
					return false;
		return true;
	}

	bool Model::isAllEnemyShipsDead() const
	{
		for(int i = 0; i<10; ++i)
			for(int j = 0; j<10; ++j)
				if(enemy_field[i][j] != Cell::Dead)
					return false;
		return true;
	}

	RecountStatus Model::RecountReadyShips(Cell** cells)
	{
		int nextShipLength = 0;

		for (int i = 0; i < 10; i++)
		{
			for (int j = 0; j < 10; j++)
			{
				if (i != 0 && j == 0)
				{
					if (nextShipLength > 4)
						return RecountStatus::MoreThanFourLengthShip;
					nextShipLength = 0;
				}

				if (cells[i][j] == Cell::Life)
					nextShipLength++;

				if (cells[i][j] != Cell::Life)
				{
					if (nextShipLength > 4)
						return RecountStatus::MoreThanFourLengthShip;
					nextShipLength = 0;
				}
			}
		}
		nextShipLength = 0;

		for (int i = 0; i < 10; i++)
		{
			for (int j = 0; j < 10; j++)
			{
				if (i != 0 && j == 0)
				{
					if (nextShipLength > 4)
						return RecountStatus::MoreThanFourLengthShip;
					nextShipLength = 0;
				}

				if (cells[j][i] == Cell::Life)
					nextShipLength++;

				if (cells[j][i] != Cell::Life)
				{
					if (nextShipLength > 4)
						return RecountStatus::MoreThanFourLengthShip;
					nextShipLength = 0;
				}
			}
		}
		return RecountStatus::Success;
	}
	bool Model::TrySetupCell(Cell** cells, int x, int y)
	{
		cells[x][y] = Cell::Life;
		bool noLongShips		= RecountReadyShips(cells) != RecountStatus::MoreThanFourLengthShip;
		bool rightTopEmpty		= x == 9 || y == 9 || cells[x + 1][y + 1] == Cell::Empty;
		bool leftTopEmpty		= x == 9 || y == 0 || cells[x + 1][y - 1] == Cell::Empty;
		bool rightBottomEmpty	= x == 0 || y == 9 || cells[x - 1][y + 1] == Cell::Empty;
		bool leftBottomEmpty	= x == 0 || y == 0 || cells[x - 1][y - 1] == Cell::Empty;

		if (noLongShips && rightBottomEmpty && leftBottomEmpty && rightTopEmpty && leftTopEmpty)
		{
			return true;
		}
		cells[x][y] = Cell::Empty;
		return false;
	}
	TurnsResult Model::CalculateTurnResult(Cell** cells, int x, int y) const
	{
		if (cells[x][y] == Cell::Empty)
			return TurnsResult::Miss;

		auto ship = GetShipCells(cells, x, y);

		bool allIsDead = true;
		std::list<Point*>::iterator iter = ship.begin();
		for(; iter != ship.end(); ++iter)
		{
			if (!((*iter)->x == x && (*iter)->y == y) && cells[(*iter)->x][(*iter)->y] != Cell::Dead)
			{
				allIsDead = false;
				break;
			}
		}
		if (allIsDead)
			return TurnsResult::Kill;

		return TurnsResult::Hit;
	}

	int Model::ShipLineLength(Cell** cells, int x, int y, int deltaX, int deltaY) const
	{
		int result = 0;
		while (true)
		{
			x += deltaX;
			y += deltaY;
			if (x >= 10 || x < 0 || y >= 10 || y < 0 || cells[x][y] == Cell::Empty || cells[x][y] == Cell::Miss)
				return result;
			result++;
		}
	}

	std::list<Point*> Model::GetShipCells(Cell** allCells, int x, int y) const
	{
		assert(allCells[x][y] != Cell::Empty && allCells[x][y] != Cell::Miss);

		int rightCount	= ShipLineLength(allCells, x, y, 1, 0);		//width max
		int leftCount	= ShipLineLength(allCells, x, y, -1, 0);	//width min
		int topCount	= ShipLineLength(allCells, x, y, 0, 1);		//height max
		int bottomCount	= ShipLineLength(allCells, x, y, 0, -1);	//height min

		topCount++;
		rightCount++;

		std::list<Point*> result;

		for (int i = y-bottomCount; i < y+topCount; i++)
		{
			for (int j = x-leftCount; j < x+rightCount;j++)
			{
				result.push_back(new Point(j,i));
			}
		}
		return result;
	}
	void Model::setCellIfPossible(Cell ** cells, Cell cellValue, int xValue, int yValue)
	{
		if (xValue >= 0 && xValue < 10 && yValue >= 0 && yValue < 10)
			cells[xValue][yValue] = cellValue;
	}
	
	void Model::MarkShipAsDead(Cell** cells, int x, int y)
	{
		auto shipCellCoords =  GetShipCells(cells, x, y);
		auto iter = shipCellCoords.begin();
		for(; iter != shipCellCoords.end(); ++iter)
		{
			setCellIfPossible(cells, Cell::Miss, (*iter)->x + 1, (*iter)->y);
			setCellIfPossible(cells, Cell::Miss, (*iter)->x + 1, (*iter)->y+1);
			setCellIfPossible(cells, Cell::Miss, (*iter)->x + 1, (*iter)->y-1);

			setCellIfPossible(cells, Cell::Miss, (*iter)->x, (*iter)->y+1);
			setCellIfPossible(cells, Cell::Miss, (*iter)->x, (*iter)->y-1);

			setCellIfPossible(cells, Cell::Miss, (*iter)->x, (*iter)->y);
			setCellIfPossible(cells, Cell::Miss, (*iter)->x, (*iter)->y+1);
			setCellIfPossible(cells, Cell::Miss, (*iter)->x, (*iter)->y-1);
		}
		iter = shipCellCoords.begin();
		for(; iter != shipCellCoords.end(); ++iter)
		{
			cells[(*iter)->x][(*iter)->y] = Cell::Dead;
		}
	}
}