#ifndef SB_FIELD_H
#define SB_FIELD_H

#include <vector>
#include <list>
#include <string>
#include "Cell.h"
#include "Ship.h"

namespace seabattle
{

	enum class State
	{
		ST_PLACING_SHIPS = 0,
		ST_WAITING_STEP = 1,
		ST_MAKING_STEP = 2
	};

	enum class TurnsResult
    {
        Miss = 0,
        Hit = 1,
        Kill = 2
    };

	enum class RecountStatus
    {
        Success = 0,
		MoreThanFourLengthShip = 1
    };

	class Point
	{
	public:
		int x;
		int y;
		Point(int _x = 0, int _y = 0) : x(_x), y(_y) { }
	};

	class Model
	{
	public:

		Model();
		~Model();

		State getState() const;
		void setState( State );

		Kind_of_ship getStateOfPlacingShip() const;
		void setStateOfPlacingShip( Kind_of_ship );

		Cell getMyCell( int cell_x, int cell_y ) const;
		void setMyCell( int cell_x, int cell_y, Cell new_cell );
		void clearMyField();
		Cell** getMyField() const;
		std::string getMyFieldInString() const;

		Cell getEnemyCell( int cell_x, int cell_y ) const;
		void setEnemyCell( int cell_x, int cell_y, Cell cell );
		void clearEnemyField();
		Cell** getEnemyField() const;

		bool isCorrectCellCoordinates( int cell_x, int cell_y ) const;

		bool isAllMyShipsDead() const;					
		bool isAllEnemyShipsDead() const;				
		RecountStatus RecountReadyShips(Cell**);		
		bool TrySetupCell(Cell**, int x, int y);

	private:

		bool isMyCellEmpty( int cell_x, int cell_y ) const;
		bool isMyCellLife( int cell_x, int cell_y ) const;
		bool isMyCellMiss( int cell_x, int cell_y ) const;
		bool isMyCellDead( int cell_x, int cell_y ) const;
		void resetCurrentNumberOfDecks();
		void incrementCurrentNumberOfDecks();
		int getCurrentNumberOfDecks() const;

		bool isAnyShipAroundThisCell( int cell_x, int cell_y ) const;
		bool isAnotherShipAroundThisCell( int cell_x, int cell_y, Kind_of_ship current_ship) const;

		TurnsResult CalculateTurnResult(Cell**, int cell_x, int cell_y) const;		
		int ShipLineLength(Cell**, int x, int y, int deltaX, int deltaY) const;		
		std::list<Point*> GetShipCells(Cell**, int x, int y) const;				
		void MarkShipAsDead(Cell**, int x, int y);								
		void setCellIfPossible(Cell**, Cell, int, int);

	private:

		Cell **my_field;
		Cell **enemy_field;
		State state;
		Ships *my_ships;
		Ships *enemy_ships;
		Kind_of_ship state_of_placing_ship;
		int current_number_of_decks;

	};
}

#endif