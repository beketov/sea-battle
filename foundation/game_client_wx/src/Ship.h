#ifndef SB_SHIP_H
#define SB_SHIP_H

#include <vector>

namespace seabattle
{
	typedef std::vector<std::pair<int,int>> my_list_of_ships;
	class Ships
	{
	public:
		my_list_of_ships *length_count_ships_array;

	public:
		Ships()
		{
			length_count_ships_array = new my_list_of_ships(); // { { 1, 4 }, { 2, 3 }, { 3, 2 }, { 4, 1 } };
			length_count_ships_array->push_back(std::make_pair(1,4));
			length_count_ships_array->push_back(std::make_pair(2,3));
			length_count_ships_array->push_back(std::make_pair(3,2));
			length_count_ships_array->push_back(std::make_pair(4,1));
		}

		bool Filled()
		{
			bool bRezult;
			auto iter = length_count_ships_array->begin();
			for(;iter!=length_count_ships_array->end(); ++iter)
			{
				if(iter->second==0)
					return true;
			}
			return false;
		}
	};
}
#endif