#include "canvas2d.h"


namespace seabattle
{

    void Canvas2D::set_brush_color(int r, int g, int b)
    {
        do_set_brush_color(r, g, b);
    }
    
    void Canvas2D::set_pen_color(int r, int g, int b)
    {
        do_set_pen_color(r, g, b);
    }
    
    void Canvas2D::set_pen_width(int v)
    {
        do_set_pen_width(v);
    }
    
    void Canvas2D::draw_rectangle(int x1, int y1, int x2, int y2)
    {
        do_draw_rectangle(x1, y1, x2, y2);
    }
    
    void Canvas2D::draw_ellipse(int x, int y, int width, int height)
    {
        do_draw_ellipse(x, y, width, height);
    }

    void Canvas2D::begin_paint()
    {
        do_begin_paint();
    }
    
    void Canvas2D::end_paint()
    {
        do_end_paint();
    }

    void Canvas2D::translate(int dx, int dy)
    {
        do_translate(dx, dy);
    }

}