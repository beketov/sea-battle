#ifndef CH_CANVAS2D_H
#define CH_CANVAS2D_H

namespace seabattle 
{
    class Canvas2D
    {
    public:
        virtual ~Canvas2D() {}

        void set_brush_color(int r, int g, int b);
        void set_pen_color(int r, int g, int b);
        void set_pen_width(int v);
        void draw_rectangle(int x1, int y1, int x2, int y2);
        void draw_ellipse(int x, int y, int width, int height);

        void begin_paint();
        void end_paint();

        void translate(int dx, int dy);

    private: // Why private virtual? Please refer to: http://www.gotw.ca/publications/mill18.htm
        virtual void do_set_brush_color(int r, int g, int b) = 0;
        virtual void do_set_pen_color(int r, int g, int b)  = 0;
        virtual void do_set_pen_width(int v) = 0;
        virtual void do_draw_rectangle(int x1, int y1, int x2, int y2) = 0;
        virtual void do_draw_ellipse(int x, int y, int width, int height) = 0;
        virtual void do_translate(int dx, int dy) = 0;

        virtual void do_begin_paint() = 0;
        virtual void do_end_paint() = 0;
    };


}

#endif // CH_CANVAS2D_H
