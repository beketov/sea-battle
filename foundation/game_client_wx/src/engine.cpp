#include <cassert>

#include "engine.h"


namespace seabattle
{

    Engine::Engine()
    {

    }
    
    Engine::~Engine()
    {
    }

    void Engine::run(Game& game)
    {
        do_run();                
    }

    EngineWindow* Engine::init_window()
    {
        return do_init_window();
    }


}