#ifndef CH_ENGINE_H
#define CH_ENGINE_H

#include "engine_window.h"

namespace seabattle
{
    class Game;

    // Represents platform for game. Encapsulates input, output, timers, etc.. 
    // This one, EngineWindow and Canvas2D are components that have to be implemented 
    // for each new platform.
    class Engine
    {
    public:
        Engine();
        virtual ~Engine();

        void run(Game& game);
        EngineWindow* init_window();

    private:
        virtual EngineWindow* do_init_window() = 0;
        virtual void do_run() = 0;

    private:
        Engine(const Engine&);
        Engine& operator=(const Engine&);

        static Engine& instance();
      
        Game* _game;        
    }; 
}

#endif // CH_ENGINE_H

// rahsal4free