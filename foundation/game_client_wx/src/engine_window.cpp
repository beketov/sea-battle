#include <cassert>

#include "engine_window.h"

namespace seabattle
{
    EngineWindow::EngineWindow()
        : _inframe(false)
    {     
    }
    
    EngineWindow::~EngineWindow()
    {
    }

    void EngineWindow::begin_frame()
    {
        assert(!_inframe);
        do_begin_frame();        
        _inframe = true;        
    }

    void EngineWindow::end_frame()
    {
        assert(_inframe);
        do_end_frame();
        _inframe = false;
    }

    Canvas2D& EngineWindow::get_canvas()
    {
        return do_get_canvas();
    }
    
    InputSource& EngineWindow::get_input()
    {
        return do_get_input();
    }

}

