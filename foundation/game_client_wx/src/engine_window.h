#ifndef CH_ENGINE_WINDOW_H
#define CH_ENGINE_WINDOW_H

#include "canvas2d.h"
#include "input_source.h"

namespace seabattle 
{
    class EngineWindow
    {
    public:        
        EngineWindow();
        virtual ~EngineWindow();

        void begin_frame();
        void end_frame();

        Canvas2D& get_canvas();
        InputSource& get_input();

    private:
        virtual void do_begin_frame() = 0;
        virtual void do_end_frame() = 0;
        virtual Canvas2D& do_get_canvas() = 0;
        virtual InputSource& do_get_input() = 0;

        bool _inframe;
    };  
    
}

#endif // CH_ENGINE_WINDOW_H

