#include <iostream>
#include <Poco/Logger.h>
#include <QtWidgets/QApplication>

#include "game.h"
#include "engine.h"
#include "qtengine.h"

int main(int argc, char* argv[])
{
    std::cout << "Creating engine.." << std::endl;
    seabattle::Engine* engine = new seabattle::QtEngine(argc, argv);
    std::cout << "Initializing window.." << std::endl;
    auto window = engine->init_window();
    std::cout << "Creating game.." << std::endl;
    seabattle::Game* game = new seabattle::Game(window->get_input());
    std::cout << "Starting game.." << std::endl;
    engine->run(*game);
    std::cout << "Finished" << std::endl;
}
