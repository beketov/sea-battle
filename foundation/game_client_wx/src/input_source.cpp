#include "input_source.h"

namespace seabattle
{
    void InputSource::clear_pending_events()
    {
        _mouse_events.clear();
        _keyboard_events.clear();
    }
    
    void InputSource::add_keyboard_event(ControlEventPtr event)
    {
        _keyboard_events.push_back(event);
    }
    
    void InputSource::add_mouse_event(ControlEventPtr event)
    {
        _mouse_events.push_back(event);
    }

    ControlEvent::Type ControlEvent::type() const
    {
        return _type;
    }

    MouseEvent::Button MouseEvent::button() const
    {
        return _button; 
    }

    MouseEvent::MouseEvent(Button button_pressed)
        : _button(button_pressed)
    {
    }

    void MouseEvent::set_button(Button button_pressed)
    {
        _button = button_pressed;
    }

    KeyboardEvent::KeyboardEvent(Key key)
        : _key(key)
    {        
    }

    void KeyboardEvent::set_key(Key key)
    {
        _key = key;
    }

    KeyboardEvent::Key KeyboardEvent::key() const
    {
        return _key;
    }

    void InputSource::set_mouse_pos(int mx, int my)
    {
        _mouse_x = mx;
        _mouse_y = my;
    }

    void InputSource::get_mouse_pos(int& out_x, int& out_y)
    {
        out_x = _mouse_x;
        out_y = _mouse_y;
    }

}