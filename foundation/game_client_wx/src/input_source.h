#ifndef CH_INPUT_SOURCE_H
#define CH_INPUT_SOURCE_H
#include <list>
#include <memory>

namespace seabattle
{    
    
    class ControlEvent
    {
    public:        

        enum Type { Type_Invalid, Type_Keyboard, Type_Mouse };

        Type type() const;
    
    private:
        Type _type;
    };
    
    typedef std::shared_ptr<ControlEvent> ControlEventPtr;


    // Class MouseEvent 
    class MouseEvent : public ControlEvent
    {
    public:
        enum Button { Button_Invalid, Button_Left, Button_Right };
        
        MouseEvent(Button button_pressed = Button_Invalid);
        
        void set_button(Button button_pressed);        
        
        Button button() const;
    
    private:
        Button _button;
    };

    
    // class KeyboardEvent 
    class KeyboardEvent : public ControlEvent
    {
    public:
        enum Key { Key_None, Key_ESC, Key_SPACE };
        
        KeyboardEvent(Key key = Key_None);
        void set_key(Key key);

        Key key() const;
    
    private:
        Key _key;
    };
    

    // class InputSource
    class InputSource
    {
    public:
        typedef std::list<ControlEventPtr> EventsContainer;      
        typedef EventsContainer::const_iterator const_iterator;
        
        const_iterator mouse_events_begin() const;
        const_iterator mouse_events_end() const;

        const_iterator keyboard_events_begin() const;
        const_iterator keyboard_events_end() const;

        void clear_pending_events();
        void add_keyboard_event(ControlEventPtr event);
        void add_mouse_event(ControlEventPtr event);

        void set_mouse_pos(int mx, int my);
        
        void get_mouse_pos(int& out_x, int& out_y);

    private:
        EventsContainer _mouse_events;
        EventsContainer _keyboard_events;

        // holds cursor position of last mouse event
        int _mouse_x;
        int _mouse_y;
    };

    inline InputSource::const_iterator InputSource::mouse_events_begin() const
    {
        return _mouse_events.begin();
    }
    
    inline InputSource::const_iterator InputSource::mouse_events_end() const
    {
        return _mouse_events.end();
    }

    inline InputSource::const_iterator InputSource::keyboard_events_begin() const
    {
        return _keyboard_events.begin();
    }
    
    inline InputSource::const_iterator InputSource::keyboard_events_end() const
    {
        return _keyboard_events.end();
    }

}


#endif // CH_INPUT_SOURCE_H
