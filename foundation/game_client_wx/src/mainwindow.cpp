#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMessageBox>
#include <QWidget>
#include <QtGui/QMouseEvent>
#include <QtGui/QPainter>


CanvasWidget::CanvasWidget(QWidget* parent /* = 0 */)
    : QWidget(parent)
{
}

void CanvasWidget::paintEvent(QPaintEvent* event)
{
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    newgame_action(0),
    game_menu(0)
{    
    create_actions();
    create_menus();
    setWindowTitle("Checkers");
    
    //QVBoxLayout * layout = new QVBoxLayout(this);
    //layout->addWidget(new CanvasWidget(this));
    //setLayout(layout);
    resize(800,600);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::create_actions()
{
    newgame_action = new QAction("New", this);
    exit_action = new QAction("Exit", this);
    connect(newgame_action, SIGNAL(triggered()), SLOT(on_new_game()));
    connect(exit_action, SIGNAL(triggered()), SLOT(on_exit_game()));
}

void MainWindow::create_menus()
{
    game_menu = menuBar()->addMenu(tr("&Game"));
    game_menu->addAction(newgame_action);
    game_menu->addSeparator();
    game_menu->addAction(exit_action);
}

void  MainWindow::on_new_game()
{
    QMessageBox::information(this, "New game action", "You are going to start new game");
}

void MainWindow::on_exit_game()
{
    QMessageBox::information(this, "Exit game action", "You are going to exit");
    // Closing main windows leads to closing application
    close();
}

void MainWindow::mousePressEvent(QMouseEvent* event)
{
    bool left = (event->buttons() & Qt::LeftButton) > 0;    
    if (left)
    {
        qDebug("Left mouse button pressed! At (%d,%d)", event->pos().x(), event->pos().y());        
    }
}

void MainWindow::paintEvent(QPaintEvent* event)
{
    QPainter painter(this);     
    int side = qMin(width(), height());
    int x = (width() - side / 2);
    int y = (height() - side / 2);
    painter.setViewport(x, y, side, side);
    //painter.setWindow(QRect(0, 0, 100, 100));   
    //painter.translate(-10, 0);
    painter.setPen(QPen(Qt::red,1,Qt::SolidLine));
    painter.drawLine(0,0, width(), height()); 
}
