#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QAction>
#include <QMenu>
#include <QLayout>

namespace Ui {
class MainWindow;
}

class CanvasWidget;

class CanvasWidget : public QWidget
{
    Q_OBJECT
public:
    CanvasWidget(QWidget* parent = 0);
protected:
    void paintEvent(QPaintEvent* event);
};



class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    
private slots:
    void on_new_game();
    void on_exit_game();

protected:        
    void mousePressEvent(QMouseEvent* event);    
    void paintEvent(QPaintEvent* event);

private:
    void create_actions();
    void create_menus();

    Ui::MainWindow *ui;
    QMenu* game_menu;
    QAction* newgame_action;
    QAction* exit_action;
    CanvasWidget* canvas;
    QLayout* layout;    
};

#endif // MAINWINDOW_H
