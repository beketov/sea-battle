#include <QtCore/QDebug>
#include <cassert>



#include "qtcanvas2d.h"

namespace seabattle 
{

    QtCanvas2D::QtCanvas2D(QWidget* parent)
        : _parent(parent)
        , _painter(NULL)
        , _buffer(NULL)
    {
    }
    
    QtCanvas2D::~QtCanvas2D()
    {
        if (_painter)
        {
            delete _painter;
        }
    }

    void QtCanvas2D::attach_buffer(QPixmap* buffer)
    {
        assert(_buffer == NULL);
        _buffer = buffer;
        _painter = new QPainter(_buffer);        
    }

    void QtCanvas2D::detach_buffer()
    {
        assert(_buffer != NULL);
        assert(_painter != NULL);        
        delete _painter;
        _painter = NULL;
        _buffer = NULL;
    }

    void QtCanvas2D::do_set_brush_color(int r, int g, int b)
    {
        _brush.setColor(qRgb(r, g, b));
    }
    
    void QtCanvas2D::do_set_pen_color(int r, int g, int b)
    {
        _pen.setColor(qRgb(r, g, b));
    }
    
    void QtCanvas2D::do_set_pen_width(int v)
    {
        _pen.setWidth(v);
    }
    
    void QtCanvas2D::do_draw_rectangle(int left, int top, int width, int height)
    {
        assert(_painter != NULL);
        _painter->drawRect(left, top, width, height);
    }
    
    void QtCanvas2D::do_draw_ellipse(int x, int y, int width, int height)
    {
        assert(_painter != NULL);
        _painter->drawEllipse(x, y, width, height);
    }

    void QtCanvas2D::do_begin_paint()
    {        
    }
    
    void QtCanvas2D::do_end_paint()
    {
        assert(_parent != NULL);
        _parent->update();
    }
    
    void QtCanvas2D::do_translate(int dx, int dy)
    {
        _painter->translate(dx, dy);
    }
}