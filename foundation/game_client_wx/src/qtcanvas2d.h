#ifndef CH_QTCANVAS2D_H
#define CH_QTCANVAS2D_H
#include <QtWidgets/QWidget>
#include <QtGui/QPainter>
#include <QtGui/QPixmap>
#include <QtGui/QBrush>
#include <QtGui/QPen>


#include "canvas2d.h"

namespace seabattle 
{
    class QtCanvas2D : public Canvas2D
    {
    public:
        QtCanvas2D(QWidget* parent);
        ~QtCanvas2D();

        // Set a buffer which is used as canvas. Client code still responsible for project deletion.
        void attach_buffer(QPixmap* buffer);        
        void detach_buffer();

    private:
        virtual void do_set_brush_color(int r, int g, int b);
        virtual void do_set_pen_color(int r, int g, int b);
        virtual void do_set_pen_width(int v);
        virtual void do_draw_rectangle(int x1, int y1, int x2, int y2);
        virtual void do_draw_ellipse(int x, int y, int width, int height);
        
        virtual void do_begin_paint();
        virtual void do_end_paint();
        
        virtual void do_translate(int dx, int dy);
    private:
        QWidget* _parent;
        QPixmap* _buffer;
        QPainter* _painter;
        QBrush _brush;
        QPen _pen;
    };


}

#endif // CH_QTCANVAS2D_H
