#include <cassert>
#include <iostream>

#include "qtengine.h"
#include "qtengine_window.h"


namespace seabattle 
{

    QtEngine::QtEngine(int argc, char* argv[])        
        : _app(new QApplication(argc, argv))
        , _window(new QtEngineWindow())
        , _timer(new QTimer())
    {        
        _timer->setSingleShot(false);
        connect(_timer, SIGNAL(timeout()), this, SLOT(idleFunc()));
    }

    void QtEngine::idleFunc()
    {    
    }

    QtEngine::~QtEngine()
    {
        if (_window)
        {
            delete _window;
        }
        if (_app)
        {
            delete _app;
        }
        if (_timer)
        {
            delete _timer;
        }
    }
    
    EngineWindow* QtEngine::do_init_window()
    {
        // create and return (requires changes in protocol)platform specific window
        return _window;
    }

    void QtEngine::do_run()
    {        
        assert(_window != nullptr);
        assert(_timer != nullptr);
        _window->show();
        _timer->start(1);
        _app->exec();        
    }

}