#ifndef CH_QTENGINE_H
#define CH_QTENGINE_H

#include <QtWidgets/QApplication>
#include <QtCore/QTimer>

#include "engine.h"
#include "qtengine_window.h"

namespace seabattle 
{
    class QtEngine: public QObject, public Engine
    {
        Q_OBJECT
    public:
        QtEngine(int argc, char* argv[]);
        ~QtEngine();

    
    private:
        virtual EngineWindow* do_init_window();
        virtual void do_run();
    
    private slots:
        void idleFunc();


    private:
        QApplication* _app;
        QtEngineWindow* _window;
        QTimer* _timer;
        
    };
}



#endif // CH_QTENGINE_H
