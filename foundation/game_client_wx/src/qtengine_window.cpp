#include <cassert>
#include <iostream>

#include <QtGui/QMouseEvent>
#include <QtGui/QKeyEvent>
#include <QtGui/QPainter>

#include "qtengine_window.h"



namespace seabattle
{

    QtEngineWindow::QtEngineWindow()
        : ui(new Ui::QtEngineWindow)
        , newgame_action(0)
        , game_menu(0)
        , _canvasbuff(NULL)
        , _canvas(NULL)
        , _inframe(false)
    {        
        reinit_canvas();
        create_actions();
        create_menus();        
        setWindowTitle("Sea Battle");
        //resize(800,600);
    }

    void QtEngineWindow::create_actions()
    {
        newgame_action = new QAction("New", this);
        exit_action = new QAction("Exit", this);
        connect(newgame_action, SIGNAL(triggered()), SLOT(on_new_game()));
        connect(exit_action, SIGNAL(triggered()), SLOT(on_exit_game()));
    }

    void QtEngineWindow::create_menus()
    {
        game_menu = menuBar()->addMenu(tr("&Game"));
        game_menu->addAction(newgame_action);
        game_menu->addSeparator();
        game_menu->addAction(exit_action);
    }

    QtEngineWindow::~QtEngineWindow()
    {
        delete_objects();
    }

    void QtEngineWindow::delete_objects()
    {
        if (_canvas)
        {
            delete _canvas;
            _canvas = NULL;
        }
        if (_canvasbuff)
        {
            delete _canvasbuff;
            _canvasbuff = NULL;
        }
    }

    Canvas2D& QtEngineWindow::do_get_canvas()
    {
        if (!_canvas)
        {
            reinit_canvas();
        }
        return *_canvas;
    }

    void QtEngineWindow::do_begin_frame()
    {
        _canvas->begin_paint(); 
    }

    void QtEngineWindow::do_end_frame()
    {
        _canvas->end_paint(); // this will cause sending us paintEvent        
    }

    void QtEngineWindow::reinit_canvas()
    {
        delete _canvas;
        _canvas = new QtCanvas2D(this);
        reinit_canvasbuffer();
        _canvas->attach_buffer(_canvasbuff);
    }

    void QtEngineWindow::reinit_canvasbuffer()
    {
        delete _canvasbuff;
        _canvasbuff = new QPixmap(QMainWindow::size());
    }

    void QtEngineWindow::paintEvent(QPaintEvent *event)
    {
        QPainter painter;
        painter.begin(this);
        painter.setRenderHint(QPainter::Antialiasing);        
        painter.drawPixmap(0,0, *_canvasbuff);
        painter.end();
    }

    void QtEngineWindow::resizeEvent(QResizeEvent *event)
    {        
        _canvas->detach_buffer();
        reinit_canvasbuffer();
        _canvas->attach_buffer(_canvasbuff);
    }

    void QtEngineWindow::mouseMoveEvent(QMouseEvent *event)
    {            
        _input.set_mouse_pos(event->pos().x(), event->pos().y());
    }

    void QtEngineWindow::mousePressEvent(QMouseEvent *event)
    {        
        ControlEventPtr mevent(new MouseEvent());
        _input.add_mouse_event(mevent);
    }

    void QtEngineWindow::mouseReleaseEvent(QMouseEvent *event)
    {        
        ControlEventPtr mevent(new MouseEvent());
        _input.add_mouse_event(mevent);
    }

    void QtEngineWindow::keyPressEvent(QKeyEvent *event)
    { 
        ControlEventPtr kevent(new KeyboardEvent());
        _input.add_keyboard_event(kevent);
    }

    InputSource& QtEngineWindow::do_get_input()
    {
        return _input;
    }

    void QtEngineWindow::on_new_game()
    {

    }
    
    void QtEngineWindow::on_exit_game()
    {

    }



}