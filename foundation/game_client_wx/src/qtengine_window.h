#ifndef CH_QT_ENGINE_WINDOW_H
#define CH_QT_ENGINE_WINDOW_H

#include <QtWidgets/QAction>
#include <QtWidgets/QMenu>
#include <QtWidgets/QLayout>
#include <QtWidgets/QWidget>
#include <QtWidgets/QMainWindow>
#include <QtGui/QBrush>
#include <QtGui/QPen>
#include <QtGui/QPixmap>

#include "engine_window.h"
#include "qtcanvas2d.h"
#include "ui_qtenginewindow.h"


namespace seabattle 
{           
    // Class responsible for grabbing events and drawing scene.
    // Current implementation subscribes on events issued by Qt framework by overriding protected virtual methods
    // which then are accumulated in input source (keyboard and mouse events). Special case is overriding paintEvent 
    // which means that window system asked us to repaint itself and we just paint what is available in canvas 
    // buffer at the moment.
    // Please note, that this class is not thread-safe as it exposes references to its buffers (canvas and input source).    
    class QtEngineWindow : public QMainWindow, public EngineWindow
    {        
        Q_OBJECT
    public:        
        QtEngineWindow();
        ~QtEngineWindow();

    private:
        virtual InputSource& do_get_input();
        virtual Canvas2D& do_get_canvas();
        virtual void do_begin_frame();
        virtual void do_end_frame();

    private:
        void paintEvent(QPaintEvent *event);                           
        void resizeEvent(QResizeEvent *event);
        void keyPressEvent(QKeyEvent *event);
        void mouseMoveEvent(QMouseEvent *event);
        void mousePressEvent(QMouseEvent *event);
        void mouseReleaseEvent(QMouseEvent *event);       

        void reinit_canvas();
        void reinit_canvasbuffer();
        void delete_objects();
        void create_actions();
        void create_menus();

    private slots:
        void on_new_game();
        void on_exit_game();


    private:
        QPixmap* _canvasbuff;
        QtCanvas2D* _canvas;
        InputSource _input;
        bool _inframe;

        Ui::QtEngineWindow *ui;
        QMenu* game_menu;
        QAction* newgame_action;
        QAction* exit_action;        
        QLayout* layout;    

    };  

}

#endif // CH_QT_ENGINE_WINDOW_H
